
var lang = []

lang['es'] = []
lang['es']['state.disconnected'] = 'Desconectado'
lang['es']['header.storage'] = 'Almacenamiento'
lang['es']['data.deleted'] = 'Borrados '
lang['es']['error'] = 'Error interno'
lang['es']['alert.accept'] = 'Aceptar'
lang['es']['connection.refused'] = 'Conexión rechazada'
lang['es']['refused.cause.error'] = 'Error en el servidor'
lang['es']['refused.cause.device'] = 'Número máximo de dispositivos alcanzados'
lang['es']['refused.cause.sensors'] = 'Su dispositivo no dispone de los siguientes sensores'
lang['es']['sensors'] = []
lang['es']['sensors'].push('Acelerómetro')
lang['es']['sensors'].push('Sensor de gavedad')
lang['es']['sensors'].push('Giroscopio')
lang['es']['sensors'].push('Sensor de luz')
lang['es']['sensors'].push('Aceleración lineal')
lang['es']['sensors'].push('Campo magnético')
lang['es']['sensors'].push('Altavoces')
lang['es']['sensors'].push('Vibración')
lang['es']['sensors'].push('Camara')
lang['es']['sensors'].push('Camara frontal')
lang['es']['sensors'].push('Micrófono')
lang['es']['sensors'].push('Teclado')
lang['es']['sensors.unknow'] = 'Desconocido'
lang['es']['not.allowed'] = 'Permiso a la camara denegado ¿Desea cambiar esta configuración?'
lang['es']['qr.scanning'] = 'Escaneando QR'

lang['en'] = []
lang['en']['state.disconnected'] = 'Disconnected'
lang['en']['header.storage'] = 'Storage'
lang['en']['data.deleted'] = 'Deleted '
lang['en']['error'] = 'Internal error'
lang['en']['alert.accept'] = 'Accept'
lang['en']['connection.refused'] = 'Connection refused'
lang['en']['refused.cause.error'] = 'Server error'
lang['en']['refused.cause.device'] = 'Maximum number of devices reached'
lang['en']['refused.cause.sensors'] = 'Your device does not have the following sensors'
lang['en']['sensors'] = []
lang['en']['sensors'].push('Accelerometer')
lang['en']['sensors'].push('Gravity sensor')
lang['en']['sensors'].push('Gyroscope')
lang['en']['sensors'].push('Light sensor')
lang['en']['sensors'].push('Linear acceleration')
lang['en']['sensors'].push('Magnetic field')
lang['en']['sensors'].push('Speakers')
lang['en']['sensors'].push('Vibration')
lang['en']['sensors'].push('Camera')
lang['en']['sensors'].push('Front camera')
lang['en']['sensors'].push('Microphone')
lang['en']['sensors'].push('Keyboard')
lang['en']['sensors.unknow'] = 'Unknow'
lang['en']['not.allowed'] = 'Camera permission denied. Do you want to change this setting?'
lang['en']['qr.scanning'] = 'Scanning QR'
