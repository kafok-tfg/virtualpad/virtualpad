
const container = $('#page-container')
let waitingConnection = false

var app = {
	// Application Constructor
	initialize: function () {
		document.addEventListener('deviceready', this.onDeviceReady.bind(this), false)
	},

	// Deviceready Event Handler
	onDeviceReady: function () {

		window.screen.orientation.lock('portrait')
		VirtualPad.start(() => {

			container.append(pages['main-page'].render())
			pages['main-page'].onInit()

			let connected = false

			document.addEventListener('backbutton', (e) => {
				if (connected || waitingConnection) {
					e.preventDefault()
					VirtualPad.disconnect()
					waitingConnection = false
					$('#lds-roller').remove()
				} else {
					back('move')
				}
			})

			document.addEventListener('resume', () => VirtualPad.startSensors())
			document.addEventListener('pause', () => VirtualPad.stopSensors())


			document.addEventListener('echo', (i) => {
				let level = 0
				if (i.latency <= 250 && i.latency > 200) level = 1
				if (i.latency <= 200 && i.latency > 150) level = 2
				if (i.latency <= 150 && i.latency > 80) level = 3
				if (i.latency <= 80 && i.latency > 25) level = 4
				if (i.latency <= 25) level = 5

				$(`#search-page li[data-id='${i.addr}:${i.tcpPort}']`).remove()

				$('#search-page').append(components['result-app'](i, level))
			})

			document.addEventListener('file-icon', (file) => {
				$(`#search-page img[data-ref='${file.addr}:${file.port}']`).attr('src', file.url)
			})

			let loadController = (controller) => $('#controller-page iframe')[0].contentWindow.postMessage({ type: 'set-controller', controller }, '*')
			let currentController = ''
			document.addEventListener('set-controller', (file) => {
				currentController = file.code
				$('#controller-page').remove()
				container.append(pages['controller'].render())
			})

			document.addEventListener('request-set-controller', (file) => {
				$('#controller-page').remove()
				container.append(pages['controller'].render())
				$('#controller-page').append(components['loader'])
			})

			document.addEventListener('connect', (host) => {
				connected = true
				AndroidFullScreen.immersiveMode(() => {

					$('#main-page').css('display', 'none')
					window.screen.orientation.lock('landscape')

				}, e => showToastLong(getMessage('error')))
			})

			document.addEventListener('disconnect', (data) => {
				window.screen.orientation.unlock()
				VirtualPad.unlock()
				AndroidFullScreen.showSystemUI(() => {

					showToastShort(getMessage('state.disconnected'))
					$('#controller-page').remove()
					window.screen.orientation.lock('portrait')
					$('#search-page ul').html('')
					$('#main-page').css('display', 'block')
					connected = false

				}, e => showToastLong(getMessage('error')))

				currentController = ''
				$('#controller-page').remove()
			})

			window.addEventListener('message', event => {
				if (event.data.type == 'load') {
					if (currentController.length > 0)
						loadController(currentController)

					$('#lds-roller').remove()
				} else if (event.data.type == 'controller-loaded') {
					$('#lds-roller').remove()

					$('#controller-page iframe')[0].contentWindow.navigator.camera = {
						getPicture: navigator.camera.getPicture
					}

					$('#controller-page iframe')[0].contentWindow.Camera = Camera
				}
			})

			document.addEventListener('connection-refused', (event) => {
				waitingConnection = false
				$('#lds-roller').remove()

				let text = ''
				if (event.cause == 1)
					text = getMessage('refused.cause.error')
				else if (event.cause == 2)
					text = getMessage('refused.cause.device')
				else {
					text = getMessage('refused.cause.sensors') + ':<br><ul>'
					for (const sensor of event.sensors) {
						const sensorName = getMessage('sensors')[sensor]
						text += '<li>' + (sensorName ? sensorName : getMessage('sensors.unknow')) + '</li>'
					}
					text += '</ul>'
				}

				container.append(components['alert'](getMessage('connection.refused'), text, getMessage('alert.accept')))
			})

			document.addEventListener('data', (data) => {
				$('#controller-page iframe')[0].contentWindow.postMessage({ type: 'data', data: { data: data.data, idSensor: data.idSensor } }, '*')
			})

			document.addEventListener('send-file', (data) => {
				$('#controller-page iframe')[0].contentWindow.postMessage({ type: 'send-file', file: { data: data.data, mimeType: data.mimeType, name: data.name } }, '*')
			})

			document.addEventListener('file', (data) => {
				$('#controller-page iframe')[0].contentWindow.postMessage({ type: 'file', file: { data: data.data, mimeType: data.mimeType, name: data.name } }, '*')
			})

			document.addEventListener('error', (error) => {
				waitingConnection = false
				$('#lds-roller').remove()
			})


		}, (e) => {
			showToastLong(getMessage('error'))
			navigator.app.exitApp()
		})
	}
}

app.initialize()


// Navigation --------

let navigation = []
let currentPage = 'main-page'

function go(target, animation) {
	container.append(pages[target].render())
	pages[target].onInit()
	let curr = $('#' + currentPage)
	let tar = $('#' + target)

	pages[currentPage].onLeave()
	curr.addClass(animation + '-out')
	tar.addClass(animation + '-in')

	curr.on('animationend', () =>  {
		if (!animation.endsWith('-back'))
			navigation.push(currentPage)

		currentPage = target

		if (target != 'qr-scanner')
			document.getElementsByTagName('html')[0].classList.remove('scanning-html')

		if (tar.attr('class')) {
			$.each(tar.attr('class').split(/\s+/), (index, item) => {
				if (item.endsWith('-back-in')) {
					tar.removeClass(item)
				}
			})
		}

		curr.remove()

		pages[target].postRender()
	})
}

function back(animation) {
	if (navigation.length == 0)
		navigator.app.exitApp()

	let last = navigation[navigation.length-1]
	navigation = navigation.slice(0, navigation.length-1)
	go(last, animation + '-back')
}


// Utils -------

function showToastLong(text) {
	window.plugins.toast.showWithOptions({
		message: text,
		duration: 'long',
		position: 'bottom',
		addPixelsY: -24
	})
}

function showToastShort(text) {
	window.plugins.toast.showWithOptions({
		message: text,
		duration: 'short',
		position: 'bottom',
		addPixelsY: -24
	})
}

function getMessage(msg) {
	let res = lang[(navigator.language || navigator.userLanguage).substring(0, 2)]
	if (res) {
		res = res[msg]
		if (res)
			return res
	}

	return ''
}


// Events -------

function scan() {
	waitingConnection = false
	$('#alert').remove()
	$('#lds-roller').remove()
	VirtualPad.scan()
}

function connect(elem) {
	waitingConnection = true
	container.append(components['loader']())

	var li = $(elem)
	currentController = ''
	VirtualPad.connect(li.data('addr'), parseInt(li.data('port')), () => {}, (e) => {
		waitingConnection = false
		$('#lds-roller').remove()
	})
}


// QR ----------

function scanQR() {
	QRScanner.prepare((err, status) => {
		if (err || !status.authorized) {
			if (window.confirm(getMessage('not.allowed')))
				QRScanner.openSettings()

			return
		}

		document.getElementsByTagName('html')[0].classList.add('scanning-html')
		go('qr-scanner', 'move')
	})
}
