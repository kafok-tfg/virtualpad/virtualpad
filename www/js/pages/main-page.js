
pages['main-page'] = new class extends Page {

	onInit() {
		VirtualPad.scan(e => showToastLong(getMessage('error')))
	}

	render() {
		return `

			<div id="main-page" class="page">
				<div class="header">
					<div>VirtualPad</div>
					<div class="button" onclick="scan()">
						<i class="fas fa-sync-alt"></i>
					</div>
				</div>

				<div class="container">
					<ul id="search-page">
					</ul>
				</div>

				<div class="config c-primary" onclick="scanQR()">
					<i class="fas fa-qrcode text-hoverable"></i>
				</div>

				<div class="config c-secundary" onclick="go('manage-storage', 'move')">
					<i class="fas fa-file-alt"></i>
				</div>
			</div>

		`
	}
}
