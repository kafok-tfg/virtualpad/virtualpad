
pages['qr-scanner'] = new class extends Page {

	onInit() {
		$('#header_qr').text(getMessage('qr.scanning'))

		var size = $('#qr-scanner').width()*0.75
		$('.container-qr div').css('width', `${size}px`)
		$('.container-qr div').css('height', `${size}px`)
	}

	postRender() {
		QRScanner.scan((err, data) => {
			if (data) {
				var obj = JSON.parse(data)
				VirtualPad.scanDirect(obj.name, obj.ips, obj.tcpPort, obj.udpPort, e => console.error(e))
			}

			VirtualPad.vibrate(80)
			back('move')
		})

		QRScanner.show()
	}

	render() {
		return `

			<div id="qr-scanner" class="page">
				<div class="header">
					<div id="header_qr">
					</div>
				</div>

				<div class="container container-qr">
					<div></div>
				</div>
			</div>

		`
	}

	onLeave() {
		$('.container-qr').css('background', '#FFF')
		QRScanner.destroy()
	}

}
