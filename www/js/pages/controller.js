
pages['controller'] = new class extends Page {

	render() {
		return `

			<div id="controller-page" class="page" style="width: 100vw; height: 100vh">
				<iframe style="width: 100%; height: 100%; border: none" src="controller.html"></iframe>
			</div>

		`
	}
}
