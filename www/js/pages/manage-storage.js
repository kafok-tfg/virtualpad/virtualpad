
pages['manage-storage'] = new class extends Page {

	onInit() {
		$('#header_storage').text(getMessage('header.storage'))
	}

	postRender() {
		const appList = $('#app-list')
		VirtualPad.listAppsData(
			apps => {
				for (const app of apps)
					appList.append(components['result-storage'](app))
			},
			e => showToastLong('Error: ' + e)
		)

	}

	render() {
		return `

			<div id="manage-storage" class="page">
				<div class="header">
					<div id="header_storage">
					</div>
				</div>

				<div class="container">
					<ul id="app-list">
					</ul>
				</div>
			</div>

		`
	}

}
