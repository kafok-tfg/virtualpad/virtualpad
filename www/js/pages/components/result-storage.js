
components['result-storage'] = (params) => {

	let url = params.name == 'Global' ? 'assets/global.png' : 'assets/default-icon.png'
	if(params.icon) {
		var buf = new ArrayBuffer(params.icon.data.length / 2)
		var bytes = new Uint8Array(buf)
		for (var i = 0; i < params.icon.data.length; i += 2)
			bytes[i / 2] = parseInt(params.icon.data.slice(i, i + 2), 16)

		url = URL.createObjectURL(new Blob([bytes], { type: params.icon.mimeType }))
	}

	let size = Math.round(params.size*100)/100

	return `

	<li data-name="${params.name}">
		<div class="img">
			<img src="${url}">
		</div>
		<div class="info">
			<div>
				<span class="name">
					${params.name}
				</span>
				<span class="dir">
					${size}${params.measure}
				</span>
				<span class="dir">
					${new Date(params.lastModified).toLocaleDateString()}
				</span>
			</div>
		</div>

		<div class="button" onclick="removeResultApp('${params.name}', '${size}${params.measure}')">
			<i class="fas fa-trash-alt"></i>
		</div>
	</li>
	<li class="separator" data-name="${params.name}"></li>

`}

function removeResultApp(app, size) {
	VirtualPad.deleteAppData(app, () => {
		showToastShort(getMessage('data.deleted') + ' ' + size)
		$(`*[data-name="${app}"]`).remove()
	}, e => showToastLong('Error: ' + e))
}
