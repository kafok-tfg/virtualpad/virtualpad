components['alert'] = (title, content, accept) => `

	<div id="alert" class="alert">
		<div>
			<div>${title}</div>
			<div>${content}</div>
			<div>
				<span onclick="$('#alert').remove()">${accept}</span>
			</div>
		</div>
	</div>

`
