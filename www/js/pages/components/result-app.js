
components['result-app'] = (i, level) => `

	<li data-id="${i.addr}:${i.tcpPort}" data-addr="${i.addr}" data-port="${i.tcpPort}" onclick="connect(this)">
		<div class="img">
			<img data-ref="${i.addr}:${i.tcpPort}" src="assets/default-icon.png">
		</div>
		<div class="info">
			<div>
				<span class="name">
					${i.name}
				</span>
				<span class="dir">
					${i.addr}:${i.tcpPort}
				</span>
			</div>
			<div class="signal">
				<div class="lv-${level}">
					<i class="fas fa-signal"></i>
				</div> ${i.latency}ms
			</div>
		</div>
	</li>
	<li class="separator" data-id="${i.addr}:${i.tcpPort}" data-addr="${i.addr}"></li>

`
