## VirtualPad

### Configuración del entorno

1. Prepararemos un directorio que será nuestro workspace, donde clonaremos este repositorio y el el proyecto del backend. 
2. Ejecutaremos ```npm install``` en ambos proyectos.
3. Debemos tener instalada la dependencia global de cordova: ```npm install -g cordova```
4. Agregaremos la paltaforma de Android a este proyecto con ```cordova platform add android```

### Guia de desarrollo

* Debe tener instalado tanto un entorno de *node* y *npm* como el SDK de Android. Use el emulador con el que se sienta más comodo.
* Si hace cambios en el backend debe volver a compilar el proyecto con el comando ```npm run debug```.
* Para compilar y obtner el *.apk* debe usar los comandos ```npm run build:dev``` ó ```npm run build:prod```. Tenga en cuenta que la *.apk* 
lista para producción debe estar firmada para poder instalarla en un disposivo.
* Para ejecutar el proyecto en un emulador o en un dispositvo conectado por USB use ```cordova run android```.